import React from "react";
import styles from "../../styles/Home.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { useRouter } from "next/router";
import { styled, alpha } from '@mui/material/styles';
import axios from "axios";
import Image from 'next/image';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import EditIcon from '@mui/icons-material/Edit';
import Divider from '@mui/material/Divider';
import ArchiveIcon from '@mui/icons-material/Archive';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import {
	faBookOpen,
	faCog,
	faHeart,
	faRocket,
	faSignOutAlt,
	faTachometerAlt,
} from "@fortawesome/free-solid-svg-icons";


const StyledMenu = styled((props) => (
	<Menu
	  elevation={0}
	  anchorOrigin={{
		vertical: 'bottom',
		horizontal: 'right',
	  }}
	  transformOrigin={{
		vertical: 'top',
		horizontal: 'right',
	  }}
	  {...props}
	/>
  ))(({ theme }) => ({
	'& .MuiPaper-root': {
	  borderRadius: 2,
	  marginTop: theme.spacing(1),
	  minWidth: 150,
	  color:
		theme.palette.mode === 'light' ? '#6B51DF' : theme.palette.grey[100],
	  boxShadow:
		'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
	  '& .MuiMenu-list': {
		padding: '2px 0',
	  },
	  '& .MuiMenuItem-root': {
		'& .MuiSvgIcon-root': {
		  fontSize: 8,
		  color: theme.palette.text.secondary,
		  marginRight: theme.spacing(1.0),
		},
		'&:active': {
		  backgroundColor: alpha(
			theme.palette.primary.main,
			theme.palette.action.selectedOpacity,
		
		  ),
		  color: '#fff',
		},
	  },
	},
  }));

function LeftNavbar() {
	const [anchorEl, setAnchorEl] = React.useState(null);
	const open = Boolean(anchorEl);
	const handleClick = (event) => {
	  setAnchorEl(event.currentTarget);
	};
	const handleClose = () => {
	  setAnchorEl(null);
	};

    const router = useRouter();
    const logout =  ()  =>  {

        // const response = await axios({
        //     method: "post",
        //     url: "http://206.189.112.218/api/logout",
        //     data: {
        //       token: localStorage.getItem("AuthToken")
        //     },
        //     headers: { 
        //         "Content-Type": "application/json",
        //         "Authorization":  "Bearer "+ localStorage.getItem('AuthToken')
        //       },
        //   });
        //   if(response === 200){
            localStorage.clear();
            router.push("/");
        //   }
      
    }
	return (
		<div className={styles.navcontainer}>
			<div className={styles.logo}>
				<h2>{}</h2>
				<Image 
					width={60}
					height={30}
					src="/logo.png" 
					alt="profile" 
					className={styles.image} 
					/>
			</div>
			<div className={styles.wrapper}>
				<ul>
					<li>
						<FontAwesomeIcon
							icon={faTachometerAlt}
							style={{ width: "10px", cursor: "pointer" }}
						/>{" "}
                        <Link href="/home">
						<a href="#">Dashboard</a>
                        </Link>
					</li>
					
					
					{/* <li>
						<FontAwesomeIcon
							icon={faCog}
							style={{ width: "10px", cursor: "pointer" }}
						/>{" "}
                        <Link href="/setting">
						<a href="#"> Settings</a>
                        </Link>
					</li>
					<li>
						<FontAwesomeIcon
							icon={faSignOutAlt}
							style={{ width: "10px", cursor: "pointer" }}
						/>{" "}
						 <Link href="#">
                            <a onClick={logout}>Logout</a>
                        </Link>
					</li> */}

					<li
        id="demo-customized-button"
        aria-controls={open ? 'demo-customized-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
		style ={{ backgroundColor: "#fff" , color:"#000", fontWeight:"bold", marginLeft:"0px",  cursor: "pointer"}}
      >
                      <FontAwesomeIcon
							icon={faCog}
							style={{ width: "10px", cursor: "pointer", color: "#6B51DF", }}
						/>{"  "}
                        
						Settings      
                        
                        </li>
      <StyledMenu
        id="demo-customized-menu"
        MenuListProps={{
          'aria-labelledby': 'demo-customized-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        
         
        <MenuItem onClick={handleClose} style={{marginLeft: "20px", fontSize:'12px'}}>
         
		               <Link href="/system">
						<a href="#"> System Settings</a>
                        </Link>
        </MenuItem>
        <Divider sx={{ my: 0.5 }} />
       
        <MenuItem onClick={handleClose} style={{marginLeft: "20px", fontSize:'12px'}}>
         
		               <Link href="/setting">
						<a href="#"> General Settings</a>
                        </Link>
        </MenuItem>
      </StyledMenu>
    
	  <li>
						<FontAwesomeIcon
							icon={faSignOutAlt}
							style={{ width: "10px", cursor: "pointer" }}
						/>{" "}
						 <Link href="#">
                            <a onClick={logout}>Logout</a>
                        </Link>
					</li> 
				</ul>
			</div>
		</div>
	);
}

export default LeftNavbar;