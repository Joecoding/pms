import {useState, useEffect} from 'react';
import { 
   Button,
    Checkbox,
     Grid, Item,
   OutlinedInput, 
   Paper, 
   List, 
   ListItem,
    ListItemText, 
     Divider,
      
      Tabs, 
      Tab,
      TabPanel,
      AppBar
      } from '@material-ui/core';
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import styles from "../../styles/Home.module.css";
import { Typography } from '@mui/material';
import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container'

function ProfileUpdate() {


  const [formValue, setFormValue] = React.useState({
    cpass: "",
    pass: "",
    conp: "",
  
  });

  const [index, setIndex] = useState(0);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState("");
  const [alertHidden, setAlertHidden] = useState(false);
  const [show, setShow] = React.useState(false);
  const [showText, setShowText] = React.useState(false);
  const router = useRouter();

  const onTabClicked = (event, index) =>{
    setIndex(index);
  }



  const hideAlert = () => {
   setAlertHidden(!alertHidden);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const registerFormData = new FormData();
    registerFormData.append("current", formValue.cpass);
    registerFormData.append("password", formValue.pass);
    registerFormData.append("cpassword", formValue.conp);
    
    console.log(registerFormData);
    console.log(localStorage.getItem('id'));

  
    try {
      const response = await axios({
        method: "post",
        url: "http://206.189.112.218/api/settings/update-organization",
        data: {
          
          name: formValue.cpass,
          street: formValue.pass,
          city: formValue.conp,
     
          
         
        },
        headers: {
           "Content-Type": "application/json",
           "Authorization": "Bearer  "+ localStorage.getItem('AuthToken')
       },
      });
    

            if (response.status === 200) {
                
                router.push("/success");
              }else{
                setIsError(true)
              }
            } catch (err) {
              setIsError(true)
              setError(err.message);
              console.log('err', error);
            }

          
          };

  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });
  };

  const Panel = (props) => (
     <div hidden={props.value !== props.index}>
       {props.value === props.index &&<Typography>{props.children}</Typography>}
     </div>
  );
  return (
    <>
       <div className={styles.contentcontainer}>
			 <div className={styles.contentwrapper}>
         <h5>General Settings</h5>
       

         <Divider/>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }} position='static' >
            
            <Tabs value={index} onChange={onTabClicked} aria-label="basic tabs example">
              <Tab label="Security"  />
              <Tab label="Personal Setting"  />
            </Tabs>
            
          </Box>
          <Divider />
          <Panel value={index} index={0}>

          <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm">
            
          < Grid style={{marginTop:'30px', marginLeft: '10px'  }} sx={12}>
            <form id="form" className="form mt-auto"   onSubmit={handleSubmit} >

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '12px'}}>
            <label>Current Password  </label>
            <OutlinedInput  placeholder='Current Password'  fullWidth type="password" value={formValue.cpass} name="cpass" onChange={handleChange}  required  style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '12px'}}>
            <label>New Password </label>
            <OutlinedInput  placeholder='New Password' type="password" fullWidth required  value={formValue.pass} name="pass" onChange={handleChange} style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
             
            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '12px'}}>
            <label>Confirm New Password</label>
            <OutlinedInput  placeholder='Confirm Password' fullWidth required type="password"  value={formValue.conp} name="conp" onChange={handleChange} style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>

       

        
            <Grid style={{marginTop:'20px',  fontSize: '15px'}}>
               <Button type="submit" variant='contained'   style={{background:'#6B51DF', color: '#fff', borderRadius:'4px', fontWeight: 'bold',}} >
                Update
          </Button>

              <Button type="submit" variant='contained'   style={{background:'red', color: '#fff', borderRadius:'4px', fontWeight: 'bold', marginLeft: '10px'}} >
                Cancel
              </Button>
            </Grid>

           
            </form>
          </Grid>

          </Container>
           </React.Fragment>
          </Panel>
        

          <Panel value={index} index={1}>
          <Paper elevation={0}
         style={{ 
            padding: '20px',
             height: '200px', 
             width : '550px', 
             marginLeft: '10px',
             marginRight: '10px',
            
              }}
         
         
         > 

         </Paper>
          </Panel>

         

       


         </div>
      </div>

       
     
        
    </>
  )
}

export default ProfileUpdate