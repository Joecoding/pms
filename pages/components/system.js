import React, {useState, useEffect} from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { 
   Button,
    Checkbox,
     Grid, Item,
   OutlinedInput, 
   Paper, 
   List, 
   ListItem,
    ListItemText, 
     Divider,
      Tabs, 
      Tab,
      TabPanel,
      AppBar
      } from '@material-ui/core';
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import styles from "../../styles/Home.module.css";
import { Typography } from '@mui/material';

function System() {


  const [formValue, setFormValue] = React.useState({
    vision: "",
    street: "",
    city: "",
    state : "",
    country: "",
  });

  const [index, setIndex] = useState(0);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState("");
  const [alertHidden, setAlertHidden] = useState(false);
  const [show, setShow] = React.useState(false);
  const [showText, setShowText] = React.useState(false);
  const router = useRouter();

  const onTabClicked = (event, index) =>{
    setIndex(index);
  }



  const hideAlert = () => {
   setAlertHidden(!alertHidden);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const registerFormData = new FormData();
    registerFormData.append("name", formValue.name);
    registerFormData.append("email", formValue.email);
    registerFormData.append("password", formValue.password);
    registerFormData.append("cpassword", formValue.cpassword);
    
    console.log(formValue.vision);
    console.log(localStorage.getItem('id'));

  
    try {
      const response = await axios({
        method: "post",
        url: "http://206.189.112.218/api/settings/update-organization",
        data: {
          id: localStorage.getItem('id'),
          name: formValue.vision,
          street: formValue.street,
          city: formValue.city,
         
         
        },
        headers: {
           "Content-Type": "application/json",
           "Authorization": "Bearer  "+ localStorage.getItem('AuthToken')
       },
      });
    

            if (response.status === 200) {
                
                router.push("/success");
              }else{
                setIsError(true)
              }
            } catch (err) {
              setIsError(true)
              setError(err.message);
              console.log('err', error);
            }

          
          };

  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });
  };



  
   
 
  
  return (
    <>
       <div className={styles.contentcontainer}>
			 <div className={styles.contentwrapper}>
         <h5>System Settings</h5>
         <h5 style={{color: '#6B51DF'}}>Organization Profile </h5>

         <Divider style={{ marginB: "15px"}}/>

         <Paper elevation={0}
         style={{ 
            padding: '20px',
             height: '150px', 
             width : '550px', 
             marginLeft: '10px',
             marginRight: '10px',
            
              }}
         
         
         > 

         </Paper>
         <Divider style={{ marginTop: "15px"}}/>
          <h6> Oganization Vision</h6>
          
     <Grid style={{marginTop:'1px', marginLeft: '10px' }}>
            <form id="form" className="form mt-auto"   onSubmit={handleSubmit} >
            <label>Vision  </label>
            <Grid style={{marginTop:'2px', fontWeight: 'bold', fontSize: '12px'}}>
            
            <OutlinedInput  placeholder='Organization Name'  fullWidth type="text" value={formValue.vision} name="vision" onChange={handleChange}  required  style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
            <Divider style={{ marginTop: "10px"}}/>
            <h6> Oganization Address</h6>
            <label>Street  </label>
            <Grid style={{marginTop:'0px', fontWeight: 'bold', fontSize: '12px'}}>
            <OutlinedInput  placeholder='Street' type="text" fullWidth required  value={formValue.street} name="street" onChange={handleChange} style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
        
            <label>State </label>    <label style={{ marginLeft:'220px'}}> City </label>
            <Grid style={{marginTop:'0px', fontWeight: 'bold', fontSize: '12px'}} >
           
            <OutlinedInput  placeholder='Lagos' required type="text" value={formValue.state} name="state" onChange={handleChange} style={{height: 45, fontWeight: 'bold',}}/>
           
            <OutlinedInput  placeholder='Lagos' required type="text"  value={formValue.city} name="city" onChange={handleChange} style={{height: 45, fontWeight: 'bold', marginLeft:'20px' }}/>
           
            </Grid>
            <label>Country </label>
            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '12px'}}>
                
            <OutlinedInput  placeholder='Nigeria' required type="text" value={formValue.country} name="country" onChange={handleChange} style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
            <Divider style={{ marginTop: "15px"}}/>
        
            <Grid style={{marginTop:'20px',  fontSize: '14px'}}>
               <Button type="submit" variant='contained'  style={{background:'#6B51DF', color: '#fff', borderRadius:'4px', fontWeight: 'bold',}} >
                Save
              </Button>
            </Grid>

           
            </form>
          </Grid>
         

       


         </div>
      </div>

       
     
        
    </>
  )
}

export default System