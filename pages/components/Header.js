import React, {useEffect, useRef} from "react";
import styles from "../../styles/Home.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from 'next/image';
import {
	faBookOpen,
	faCog,
	faHeart,
	faRocket,
	faSignOutAlt,
	faTachometerAlt,
} from "@fortawesome/free-solid-svg-icons";



function Header() {
	var token;

    useRef(() => {
		token = localStorage.getItem('name');
		console.log(token);
		
			

	  });
	  
	return (
		<div className={styles.headcontainer}>
			<div className={styles.headwrapper}>
				<div className={styles.title}>
					
				</div>
				<div className={styles.profile}>
				<Image 
					width={25}
					height={25}
					src="/bell.svg" 
					alt="profile" 
					className={styles.image} 
					/>
					</div>
					<div className={styles.profile} style={{marginRight: 40}}>
					<Image 
					width={25}
					height={25}
					src="/user.svg" 
					alt="profile" 
					className={styles.image} 
					/>
				</div>
			</div>
		</div>
	);
}

export default Header;