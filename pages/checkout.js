import React from 'react';
import {  Button, Checkbox, Divider, Grid, OutlinedInput, Paper } from '@material-ui/core';
import Image from 'next/image';


const Checkout = () => {
    return (
        <>
        <div>
        <Paper elevation={10} style={{ 
           padding: '20px',
            height: '630px', 
            width : '1000px', 
            margin: '10px auto',
            backgroundImage: `url("/bgg.png")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            
             }}>

         <Paper elevation={10} style={{ 
            padding: '20px',
            height: '200px', 
            width : '500px', 
            marginLeft: '240px',
            marginRight: '10px',
            marginTop: '70px',
            background: '#6B51DF',
             }}>
            <Image src="/Vector.png" alt="Image" align="center" style={{marginTop: 60, marginLeft: 210 }}/>

          </Paper>
         
          <Paper elevation={10} style={{ 
           padding: '20px',
            height: '200px', 
            width : '500px', 
            marginLeft: '240px',
            marginRight: '10px',
            marginTop:'1px',
             }}>

           <Grid align="center">
              <h3 style={{marginLeft: 10}}>Awesome!</h3> 
              
              <Grid style={{marginTop:'15px', marginLeft:'10px',fontSize:'15px'}} align="center"> 
              <p>Your organization has been successfully created</p>
              </Grid> 

              <Grid style={{marginTop:'25px', fontWeight: 'bold', fontSize: '14px'}}> 
               <Button type="submit" variant='contained'  style={{background:'#6B51DF', color: '#fff', borderRadius:'40px' , fontWeight: 'bold',}} >
                Go to Dashboard
              </Button>
            </Grid> 
                 
            </Grid>

          </Paper>

        </Paper>
        </div>
        </>
    );
}

export default Checkout;
