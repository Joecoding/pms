import React, {useState, useEffect} from 'react';
import { Button,  Grid, OutlinedInput, Paper, Alert } from '@material-ui/core';
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Image from 'next/image';

 const Register = () => {
  const [formValue, setFormValue] = React.useState({
    name: "",
    email: "",
    password: "",
    cpassword : "",
  });
  const [isError, setIsError] = useState(false)
  const [error, setError] = useState("");
  const [alertHidden, setAlertHidden] = useState(false);
  const [show, setShow] = React.useState(false);
  const [showText, setShowText] = React.useState(false);
  const router = useRouter();

  const showPassword = () => setShow(!show);

  const showMe = ()  => setShowText(!showText);

  const hideAlert = () => {
   setAlertHidden(!alertHidden);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const registerFormData = new FormData();
    registerFormData.append("name", formValue.name);
    registerFormData.append("email", formValue.email);
    registerFormData.append("password", formValue.password);
    registerFormData.append("cpassword", formValue.cpassword);
    
    console.log(formValue.name);

    if(formValue.password !== formValue.cpassword){
      setIsError(true)
    }else{
  
    try {
      const response = await axios({
        method: "post",
        url: "http://206.189.112.218/api/register",
        data: {
          name: formValue.name,
          email: formValue.email,
          password: formValue.password,
          password_confirmation: formValue.cpassword,
         
        },
        headers: { "Content-Type": "application/json" },
      });
      console.log(response);

            if (response.status === 200) {
               
                localStorage.setItem('AuthToken', `${response.data.data.token}`);
                localStorage.setItem('email', `${formValue.email}`);
                
                router.push("/success");
              }else{
                setIsError(true)
              }
            } catch (err) {
              setIsError(true)
              setError(err.message);
              console.log('err', error);
            }

          }
          };

  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });
  };

    return (
        <>
        <div>
        <Grid>
        <Paper elevation={10} style={{ 
           padding: '20px',
            height: '650px', 
            width : '400px', 
            margin: '10px auto',
            backgroundImage: `url("/bg.png")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            
             }}>
          <Grid>
         <Paper elevation={10} style={{ 
           padding: '20px',
            height: '530px', 
            width : '350px', 
            marginLeft: '10px',
            marginRight: '10px',
            marginTop: '50px',
           
            
             }}>
            <Grid align="center">
            <Image
              src="/logo.png"
              width="90px"
              height="40px"  
              alt="Image"
              />
            <h4>Register your organization</h4>
            

          

            </Grid>
             

             <Grid style={{marginTop:'30px'}}>
            <form id="form" className="form mt-auto" onSubmit={handleSubmit} >

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}} >
            <label>Organization Name </label>
            <OutlinedInput  placeholder='Organization Name' type="text" value={formValue.name} name="name" onChange={handleChange}  fullWidth required  style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}} >
            <label>Email </label>
            <OutlinedInput  placeholder='info@company.com' value={formValue.email} name="email"  onChange={handleChange} fullWidth required type="email" style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
              
            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label>Password </label>
            <OutlinedInput  placeholder='*************' fullWidth required  value={formValue.password} type="password" name="password" onChange={handleChange}  style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>


            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}} > 
            <label> Confirm Password </label>
            <OutlinedInput  placeholder='*************' fullWidth required type="password"  value={formValue.cpassword} name="cpassword" onChange={handleChange} style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
           

            {/* {Sign In} */}
            <Grid style={{marginTop:'20px',  fontSize: '15px'}}>
               <Button type="submit"  onClick={hideAlert}  variant='contained' fullWidth  style={{background:'#6B51DF', color: '#fff', borderRadius:'4px', fontWeight: 'bold',}} >
                Register
              </Button>
            </Grid>

            <Grid style={{marginTop:'10px',  fontSize: '15px'}}>
              <label>Already have an account ?  <Link href="/"><a color="primary" style={{color: '#6B51DF'}}>Sign in</a></Link></label>
            </Grid>
            </form>
            </Grid>

         </Paper>
         </Grid>


         </Paper>
     
        </Grid>
        </div>
        </>
    );
}

export default Register;
