import React from 'react';
import {  Button,  Grid, Paper } from '@material-ui/core';
import { useRouter } from "next/router";
import Image from 'next/image';

const Success = () => {

  
  const router = useRouter();
  const Dashboard = async (e) => {
    router.push("/home");
  }

    return (
        <>
        <div>
        <Paper elevation={10} style={{ 
           padding: '20px',
            height: '630px', 
            width : '400px', 
            margin: '10px auto',
            backgroundImage: `url("/bg.png")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            
             }}>

         <Paper elevation={10} style={{ 
            padding: '20px',
            height: '200px', 
            width : '350px', 
            marginLeft: '10px',
            marginRight: '10px',
            marginTop: '70px',
            background: '#6B51DF',
             }}>
            <Image alt="Image" src="/Vector.png" align="center" style={{marginTop: 60, marginLeft: 120 }}/>

          </Paper>
         
          <Paper elevation={10} style={{ 
           padding: '20px',
            height: '200px', 
            width : '350px', 
            marginLeft: '10px',
            marginRight: '10px',
            marginTop:'1px',
             }}>

        <Grid align="center">   
              <h3 >Awesome!</h3> 
              
              <Grid style={{marginTop:'15px', marginLeft:'10px',fontSize:'15px'}} align="center"> 
              <p>Your organization has been successfully created</p>
              </Grid> 

              <Grid style={{marginTop:'25px', fontWeight: 'bold', fontSize: '14px'}}> 
               <Button type="submit" variant='contained' onClick={Dashboard}  style={{background:'#6B51DF', color: '#fff', borderRadius:'40px' , fontWeight: 'bold',}} >
                Go to Dashboard
              </Button>
            </Grid> 
                 
            
       </Grid>
          </Paper>

        </Paper>
        </div>
        </>
    );
}

export default Success;
