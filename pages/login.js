import React,{useState, useEffect, Component}  from 'react'
import { Button, Checkbox, Grid, OutlinedInput, Paper, label, FormControlLabel, FormGroup } from '@material-ui/core';
import Link from "next/link";
import axios from "axios";
import { useRouter } from "next/router";
import MessageBox from '../pages/components/messageBox';
import Image from 'next/image';
import { Alert} from '@mui/material';


export default  function Login () {

  

  const [formValue, setFormValue] = React.useState({
    email: "",
    password: "",
  });

  const [isError, setIsError] = useState(false);
  const [error, setError] = useState("");
  const [alertHidden, setAlertHidden] = useState(false);
  const [show, setShow] = React.useState(false);
  const [loading, setLoading] = useState(true)

  const showPassword = () => setShow(!show);

 
  const hideAlert = () => {
    setAlertHidden(!alertHidden);
    window.location.reload();

   };

  const handleChange = (e) => {
    e.preventDefault();
    setFormValue({
      ...formValue,
      [e.target.name]: e.target.value,
    });    
  };
 

  const router = useRouter();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const loginFormData = new FormData();
    loginFormData.append("username", formValue.email);
    loginFormData.append("password", formValue.password);

    console.log(formValue.password);
    try {
      const response = await axios({
        method: "post",
        url: "http://206.189.112.218/api/user-login",
        data: {
          email: formValue.email,
          password: formValue.password,
        },
        headers: { "Content-Type": "application/json" },
      });
      console.log(response);

      if (response.status === 200) {
        localStorage.setItem('AuthToken', response.data.data.token);
        localStorage.setItem('email', `${formValue.email}`);
        localStorage.setItem('name', response.data.data.user.name);
        localStorage.setItem('id', response.data.data.user.id);

        console.log("token", response.data.data.token)
        router.push("/home");
        setLoading(false)
      
      }else{
        setIsError(true);
        
      }
    } catch (err) {
      setIsError(true);
      setError(err.message);
      console.log('err', err.message);
    }
  };

 
if(!alertHidden){  

  return (
    
       <>
      {(() => {
 
  return (
    <>
      <Grid>
        <Paper elevation={10} style={{ 
           padding: '20px',
            height: '630px', 
            width : '400px', 
            margin: '10px auto',
            backgroundImage: `url("/bg.png")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
             }}>
         <Grid>
         <Paper elevation={10} style={{ 
           padding: '20px',
            height: '460px', 
            width : '350px', 
            marginLeft: '10px',
            marginRight: '10px',
            marginTop: '70px'
             }}>
            <Grid align="center">
             <Image
              src="/logo.png"
              width="90px"
              height="40px" 
              alt="Image" 
            />
              
                
            <h4>Sign in to your account</h4>
            
            </Grid>

            
            {isError ?
             (<Alert severity="success" onClose={hideAlert} >{`${error}.\n user details not correct.`}</Alert>) : ""}
            <div className="m-auto">
              
            <div>
              <div className="form-body">
                <form onSubmit={handleSubmit} id="form" className="form mt-auto">
                <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}} >
                 <label>Email</label>
                 <OutlinedInput  value={formValue.name} name="email" onChange={handleChange}  placeholder='hello@gmail.com' type="email" fullWidth required  style={{height: 45, fontWeight: 'bold',}}/>
                 </Grid>
                 <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}} >
                  <label>Password</label>
                  <OutlinedInput  value={formValue.password} name="password" onChange={handleChange} placeholder='*************' fullWidth required type={show ? "text" : "password"} style={{height: 45, fontWeight: 'bold',}}/>
                </Grid>
                <Checkbox
              color='primary'
              label='Remember me'
                /> 
                <label>Remember me</label>  
                <label style={{ marginLeft : 13}}>  <Link href="#">Forget password?</Link></label>
                

               <Grid style={{marginTop:'5px', fontWeight: 'bold', fontSize: '14px'}}> 
               <Button type="submit" variant='contained' fullWidth  style={{background:'#6B51DF', color: '#fff', borderRadius:'4px' , fontWeight: 'bold'}} >
                Sign In
              </Button>
              </Grid>
            <Grid style={{marginTop:'10px', fontSize: '14px'}}> 
              <label>Dont have an account?  <Link href="/register"><a color="primary" style={{color: '#6B51DF'}}>Sign up</a></Link></label>
            </Grid>
            
             </form>
            </div>
            </div>
            </div>
         </Paper>
         </Grid>
         <Grid style={{marginTop:'10px', fontSize: '14px'}}> 
         <label> Second Register<Link href="/secondRegister"><a color="primary" style={{color: '#6B51DF'}}>Sign up</a></Link></label>
         </Grid>

         </Paper>
      
     
      </Grid>
    
      </>
    
  );
 
    })()}
    </>
  
  
  );
}
return <div/>
}
