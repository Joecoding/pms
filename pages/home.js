import Head from "next/head";
import React, { useCallback, useEffect, useState, useRef} from "react";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import Content from "./components/main";
import Header from "./components/Header";
import LeftNavbar from "./components/Navbar";
import { applyAuth } from "../utils/auth";
import { useRouter } from "next/router";

export default function Home() {

	const [user, setUser] = useState({});
	const router = useRouter();

	useEffect(() => {
		
		applyAuth(router, setUser);

	  }, [user, router]);
	  
	return (
		<div className={styles.container}>
			<Head>
				<title>Organization dashboard</title>
				<meta name="description" content="Created by Balogun Joseph" />
				<link rel="icon" href="/logo.png" />
			</Head>
			<div className={styles.container}>
				<LeftNavbar />
				<Header />
				<Content />
			</div>
		</div>
	);
}