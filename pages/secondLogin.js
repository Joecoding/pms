import React from 'react';
import {  Button, Checkbox, Grid, OutlinedInput, Paper } from '@material-ui/core';
import Link from "next/link";
import Image from 'next/image';


const SecondLogin = () => {


  return (
    <>
      <Grid>
        <Paper elevation={10} style={{ 
           padding: '20px',
            height: '630px', 
            width : '1000px', 
            margin: '10px auto',
            backgroundImage: `url("/bgg.png")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
             }}>
         <Grid>
         <Paper elevation={10} style={{ 
           padding: '20px',
            height: '460px', 
            width : '500px', 
            marginLeft: '240px',
            marginRight: '10px',
            marginTop: '70px'
             }}>
            <Grid align="center">
             <Image
              src="/logo.png"
              width="90px"
              height="40px" 
              alt="Image"
            />

            <h4>Sign in to your account</h4>
            
            </Grid>
             

             <Grid style={{marginTop:'30px'}}>
             <form id="form" className="form mt-auto" >

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label>Email</label>
            <OutlinedInput  placeholder='hello@gmail.com' type="email" fullWidth required  style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label>Password</label>
            <OutlinedInput  placeholder='*************' fullWidth required type="password" style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
              
            <Checkbox
              color='primary'
              label='Remember me'
                /> 
                <label>Remember me</label>  
                <label style={{ marginLeft : 130}}>  <Link href="#">Forget password?</Link></label>

            {/* {Sign In} */}

            <Grid style={{marginTop:'5px', fontWeight: 'bold', fontSize: '14px'}}> 
               <Button type="submit" variant='contained' fullWidth  style={{background:'#6B51DF', color: '#fff', borderRadius:'4px' , fontWeight: 'bold',}} >
                Sign In
              </Button>
            </Grid>
            <Grid style={{marginTop:'15px', fontSize: '14px'}}> 
              <label>Dont have an account?  <Link href="/register"><a color="primary" style={{color: '#6B51DF'}}>Sign up</a></Link></label>
            </Grid>
            </form>
              </Grid>

         </Paper>
         </Grid>


         </Paper>
     
      </Grid>
  
    </>
  )
}

export default SecondLogin;