
import React from 'react';
import {  Button, Checkbox, Grid, OutlinedInput, Paper } from '@material-ui/core';
import Link from "next/link";
import Image from 'next/image';
function secondRegister() {
  return (
    <>
       <Grid>
        <Paper elevation={10} style={{ 
           padding: '20px',
            height: '650px', 
            width : '1000px', 
            margin: '10px auto',
            backgroundImage: `url("/bgg.png")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            
             }}>
          <Grid>
         <Paper elevation={10} style={{ 
           padding: '20px',
            height: '530px', 
            width : '500px', 
            marginLeft: '240px',
            marginRight: '10px',
            marginTop: '50px',
           
            
             }}>
            <Grid align="center">
            <Image
              src="/logo.png"
              width="90px"
              height="40px"  
              alt="Image"
            />

            <h4> Create your organization</h4>
            

          

            </Grid>
             

             <Grid style={{marginTop:'30px'}}>
            <form id="form" className="form mt-auto" >

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label>Organization Name </label>
            <OutlinedInput  placeholder='Organization Name' type="text" fullWidth required  style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>

            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label>Email </label>
            <OutlinedInput  placeholder='info@company.com' fullWidth required type="email" style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
              
            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label>Password </label>
            <OutlinedInput  placeholder='*************' fullWidth required type="password" style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>


            <Grid style={{marginTop:'10px', fontWeight: 'bold', fontSize: '14px'}}>
            <label> Confirm Password </label>
            <OutlinedInput  placeholder='*************' fullWidth required type="password" style={{height: 45, fontWeight: 'bold',}}/>
            </Grid>
           

            {/* {Sign In} */}
            <Grid style={{marginTop:'20px',  fontSize: '15px'}}>
               <Button type="submit" variant='contained' fullWidth  style={{background:'#6B51DF', color: '#fff', borderRadius:'4px', fontWeight: 'bold',}} >
                Register
              </Button>
            </Grid>

            <Grid style={{marginTop:'10px',  fontSize: '15px'}}>
              <label>Already have an account ?  <Link href="/secondLogin"><a color="primary" style={{color: '#6B51DF'}}>Sign in</a></Link></label>
            </Grid>
            </form>
            </Grid>

         </Paper>
         </Grid>


         </Paper>
     
        </Grid>
    </>
  )
}

export default secondRegister